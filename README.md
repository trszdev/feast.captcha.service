![Feast captcha service](logo.png)

## Summary

Springboot service which generates captcha images using random fonts, rotation, bulge filter parameters

## Rules

* [Kontur java intern 2018](https://kontur.ru/education/programs/intern/java2018)
* [CAPTCHA service requirements (PDF, ru)](CAPTCHAv2.pdf)

## Human testing results

2:00am, male 21yr solving 100 captchas:

* Corrects: 67
* Average solution time: 10.2321764421463 seconds

![Feast captcha service](stats.png)

## Related links

* [Strong captcha guidelines v1.2](http://www.123seminarsonly.com/Seminar-Reports/008/47584359-captcha.pdf)
* [PWNtcha](http://caca.zoy.org/wiki/PWNtcha) - samples of weak captchas
* [Bulge filter, stackoverflow](https://math.stackexchange.com/questions/266250/explanation-of-this-image-warping-bulge-filter-algorithm)

## Test

### Unittest, IT:

```bash
mvn test
```

### Playground:

```bash
mvn spring-boot:run
```

then goto `http://localhost:8080/`

### Readability tests (statistics)

goto `src/test/python36`, do `pip install -r requirements.txt`

1. obtain images running service on `localhost:8080` using `imageloader.py`
2. fill answers in `testform.py`
3. `buildcharts.py`

## Compile and run

1. `mvn package & cd target`
2. java -Dproduction -Dttl=10 -jar captcha.service.jar
