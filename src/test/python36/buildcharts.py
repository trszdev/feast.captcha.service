from matplotlib.pyplot import (figure, subplot, plot, title,
                               xlabel, ylabel, show, xticks, yticks)
from csv import reader
from tkinter.messagebox import showinfo
from glob import glob
from editdistance import eval as levenshtein


def read_stats(fname):
    with open(fname) as file:
        for row in reader(file):
            answer, fname, duration = row
            _, expected_answer, *_ = fname[:-4].split('_', 3)
            distance = levenshtein(answer, expected_answer)
            yield float(duration), distance


def draw_stats(name, series):
    figure()
    subplot(211)
    title(f'{name} solving results')
    xticks(range(1, len(series)+1))
    plot(range(1, len(series)+1), [x[0] for x in series], linestyle='-', marker='o')
    xlabel('No. of image')
    ylabel('Time for solution (ms)')
    subplot(212)
    xticks(range(1, len(series)+1))
    plot(range(1, len(series) + 1), [x[1] for x in series], linestyle='-', marker='^')
    xlabel('No. of image')
    ylabel('Word distance (Levenshtein)')


if not any(glob('*.csv')):
    showinfo('No *.csv files found', 'Please, run testform.py')
    exit()


for stats_filename in glob('*.csv'):
    stats = list(read_stats(stats_filename))
    print('Corrects:', len([x for x in stats if not x[1]]))
    print('Average solution time:', sum([x[0] for x in stats]) / len(stats))
    draw_stats(stats_filename, stats)

show()
