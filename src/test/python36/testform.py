from tkinter import Tk, Canvas, PhotoImage, Entry
from tkinter.messagebox import showinfo, askokcancel
from glob import glob
from PIL import Image, ImageTk
from csv import writer
from time import time


def append_stats(stats):
    with open('stats.csv', 'a', newline='') as file:
        writer(file).writerow(stats)


def create_root(width, height):
    root = Tk()
    width += 20
    height += 20 + 40
    root.title('Test you\'re not a robot')
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width // 2) - (width // 2)
    y = (screen_height // 2) - (height // 2)
    root.geometry(f'{width}x{height}+{x}+{y}')
    root.resizable(width=False, height=False)

    def on_close():
        if askokcancel('Final question','You wanna quit?'):
            exit()

    root.protocol('WM_DELETE_WINDOW', on_close)
    return root


if not any(glob('*.png')):
    showinfo('No *.png captcha files found', 'Please, run imageloader.py')
    exit()


for image_path in glob('*.png'):
    started = time()
    image = Image.open(image_path)
    width, height = image.size
    root = create_root(width, height)
    photo_img = ImageTk.PhotoImage(image)
    canvas = Canvas(root, width=width, height=height)
    canvas.create_image((width//2, height//2), image=photo_img, state='normal')
    canvas.place(x=10, y=10, width=width, height=height)
    entry = Entry(root, width=width)
    entry.place(x=10, y=height+20, width=width, height=20)

    def on_submit(_):
        append_stats([entry.get(), image_path, time() - started])
        root.destroy()

    entry.bind('<Return>', on_submit)
    root.after(1, entry.focus_force())
    root.mainloop()
