from requests import get, post
from shutil import copyfileobj


def o(suffix):
    return 'http://localhost:8080' + suffix


def download_image(i):
    print(f'Downloading {i}th image...')
    keypair = post(o('/client/register')).json()
    public = keypair['public']
    captcha = get(o(f'/captcha/new?public={public}')).json()
    answer, request = captcha['answer'], captcha['request']
    r = get(o(f'/captcha/image?public={public}&request={request}'), stream=True)
    with open(f'{i}_{answer}_{request}.png', 'wb') as file:
        r.raw.decode_content = True
        copyfileobj(r.raw, file)


for i in range(1, 101):
    download_image(i)
