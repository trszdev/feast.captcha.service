package com.feast.captcha.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AesGcmCipherSuiteTest {

    @Autowired
    private AesGcmCipherSuite sut;

    @Test
    public void decryptOnce() throws Exception {
        byte[] plain = Utils.randomBytes(10);
        UUID key = Utils.generateUuid();
        byte[] cipher = sut.encryptWithTtl(plain, key, 2).getOrNull();
        byte[] plain2 = sut.decryptOnce(cipher, key).getOrNull();
        byte[] plain3 = sut.decryptOnce(cipher, key).getOrNull();

        Assert.assertTrue(Arrays.equals(plain, plain2));
        Assert.assertNull(plain3);
    }

    @Test
    public void decryptOnce_ttl() throws Exception {
        byte[] plain = Utils.randomBytes(10);
        UUID key = Utils.generateUuid();
        byte[] cipher = sut.encryptWithTtl(plain, key, 1).getOrNull();
        Thread.sleep(1000);
        byte[] plain2 = sut.decryptOnce(cipher, key).getOrNull();

        Assert.assertNull(plain2);
    }

    @Test
    public void decryptWithTtl() throws Exception {
        byte[] plain = Utils.randomBytes(10);
        UUID key = Utils.generateUuid();
        byte[] cipher = sut.encryptWithTtl(plain, key, 2).getOrNull();
        byte[] plain2 = sut.decryptWithTtl(cipher, key).getOrNull();
        byte[] plain3 = sut.decryptWithTtl(cipher, key).getOrNull();

        Assert.assertTrue(Arrays.equals(plain, plain2));
        Assert.assertTrue(Arrays.equals(plain3, plain2));
    }
}