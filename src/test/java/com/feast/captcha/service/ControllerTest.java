package com.feast.captcha.service;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ControllerTest {
    @Autowired
    private MockMvc mvc;

    private JSONObject json(MockHttpServletRequestBuilder builder) throws Exception {
        String body = mvc.perform(builder).andReturn().getResponse().getContentAsString();
        return new JSONObject(body);
    }

    @Test
    public void registerClient() throws Exception {
        mvc.perform(post("/client/register"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.public").isString())
                .andExpect(jsonPath("$.secret").isString());
    }

    @Test
    public void invalidKey() throws Exception {
        mvc.perform(get("/captcha/new")
                .param("public", "not a key"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error").isString());
        mvc.perform(get("/captcha/new")
                .param("public", "7810ebec-b343-42ef-84da-a8990c6a9125"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error").isString());
    }

    @Test
    public void newCaptcha() throws Exception {
        JSONObject keyPair = json(post("/client/register"));
        mvc.perform(get("/captcha/new")
                .param("public", keyPair.getString("public")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.request").isString())
                .andExpect(jsonPath("$.answer").isString());
    }

    @Test
    public void solveCaptcha() throws Exception {
        JSONObject keyPair = json(post("/client/register"));
        JSONObject newCaptcha = json(get("/captcha/new")
                .param("public", keyPair.getString("public")));
        JSONObject solution = json(post("/captcha/solve")
                .param("public", keyPair.getString("public"))
                .param("request", newCaptcha.getString("request"))
                .param("answer", newCaptcha.getString("answer")));
        mvc.perform(get("/captcha/verify")
                .param("secret", keyPair.getString("secret"))
                .param("response", solution.getString("response")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.errorCode").isEmpty());
        mvc.perform(get("/captcha/verify")
                .param("secret", keyPair.getString("secret"))
                .param("response", solution.getString("response")))
                .andExpect(status().isBadRequest());
    }
}
