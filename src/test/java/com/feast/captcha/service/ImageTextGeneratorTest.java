package com.feast.captcha.service;

import com.feast.captcha.service.image.Feast;
import com.feast.captcha.service.datatypes.ImageSize;
import com.feast.captcha.service.image.ImageTextGenerator;
import com.feast.captcha.service.datatypes.ImageTextOptions;
import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;


public class ImageTextGeneratorTest {

    private static ImageTextOptions getOptions(int width, int height, float fontSize) {
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
        return new ImageTextOptions(ImmutableList.of(font), new ImageSize(width, height));
    }

    private static int getWhitePixels(BufferedImage image) {
        int[] count = { 0 };
        Utils.forEachPixel(image, (x, y) -> {
            if (Color.white.equals(new Color(image.getRGB(x, y))))
                count[0]++;
        });
        return count[0];
    }

    private static int getBlackPixels(BufferedImage image) {
        return image.getWidth() * image.getHeight() - getWhitePixels(image);
    }

    private final static ImageTextGenerator itg1 = Feast.getImageTextGenerator(0, .6, .1);
    private final static ImageTextGenerator itg2 = Feast.getImageTextGenerator(1, .6, .1);

    private final static ImageTextGenerator itg3 = Feast.getImageTextGenerator(0, 1, .1);


    @Test
    public void testDimensions() throws Exception {
        BufferedImage img = itg1.getTextImage("aaaa", getOptions(100, 120, 10f));

        Assert.assertEquals(100, img.getWidth());
        Assert.assertEquals(120, img.getHeight());
        Assert.assertTrue(getWhitePixels(img) > 0);
    }

    @Test
    public void testDifferentRandomIvs() throws Exception {
        BufferedImage img = itg1.getTextImage("aaaa", getOptions(100, 120, 10f));
        BufferedImage img2 = itg2.getTextImage("aaaa", getOptions(100, 120, 10f));

        Assert.assertEquals(img2.getWidth(), img.getWidth());
        Assert.assertEquals(img2.getHeight(), img.getHeight());
        Assert.assertNotEquals(getWhitePixels(img), getWhitePixels(img2));
    }

    @Test
    public void testDifferentText() throws Exception {
        BufferedImage img = itg3.getTextImage("aaaa", getOptions(100, 120, 10f));
        BufferedImage img2 = itg3.getTextImage("aaaaa", getOptions(100, 120, 10f));
        int whitePixels = getWhitePixels(img);
        int whitePixels2 = getWhitePixels(img2);
        int dw = whitePixels2 - whitePixels;

        Assert.assertTrue(dw > 0);
        Assert.assertTrue(dw < whitePixels / 3.5);
    }
}