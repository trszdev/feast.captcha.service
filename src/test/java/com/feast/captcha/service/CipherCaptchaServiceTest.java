package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.CaptchaImageParameters;
import com.feast.captcha.service.datatypes.CaptchaRequest;
import com.feast.captcha.service.datatypes.KeyPair;
import com.feast.captcha.service.datatypes.Token;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Eithers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyException;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CipherCaptchaServiceTest {

    @Autowired
    private CaptchaService sut;

    @Test
    public void createClient_secretIsntPublic() throws Exception {
        KeyPair keyPair = sut.createClient();
        Assert.assertNotEquals(keyPair.publicKey, keyPair.secretKey);
    }

    @Test
    public void createClient_keypairDiffers() throws Exception {
        KeyPair keyPair1 = sut.createClient();
        KeyPair keyPair2 = sut.createClient();

        Assert.assertNotEquals(keyPair1, keyPair2);
    }

    @Test
    public void createCaptcha_unknownKey() throws Exception {
        Exception exception = sut.createCaptcha(Utils.generateUuid()).left().getOrNull();

        Assert.assertTrue(exception instanceof KeyException);
    }

    @Test
    public void createCaptcha_requestsDiffer() throws Exception {
        KeyPair keyPair = sut.createClient();
        CaptchaRequest captchaRequest1 = sut.createCaptcha(keyPair.publicKey).getOrNull();
        CaptchaRequest captchaRequest2 = sut.createCaptcha(keyPair.publicKey).getOrNull();

        Assert.assertNotEquals(captchaRequest1, captchaRequest2);
    }

    private void scenario_solveCaptchaTwice(boolean correctFirstTime, boolean correctSecondTime) throws Exception {
        KeyPair keyPair = sut.createClient();
        CaptchaRequest captchaRequest = sut.createCaptcha(keyPair.publicKey).getOrNull();
        String answer1 = correctFirstTime ? captchaRequest.answer : "Wrong answer 1";
        String answer2 = correctSecondTime ? captchaRequest.answer : "Wrong answer 2";
        Token token1 = Eithers.getOrThrow(sut.getSolutionToken(keyPair.publicKey, captchaRequest.token, answer1));
        Token token2 = sut.getSolutionToken(keyPair.publicKey, captchaRequest.token, answer2).getOrNull();
        Boolean wasAnswered1 = Eithers.getOrThrow(sut.isAnswered(keyPair.secretKey, token1));
        Boolean wasAnswered2 = sut.isAnswered(keyPair.secretKey, token1).isRight();

        Assert.assertNotEquals(token1, token2);
        Assert.assertTrue(correctFirstTime ? wasAnswered1 : !wasAnswered1);
        Assert.assertNull(token2);
        Assert.assertFalse(wasAnswered2);
    }

    @Test
    public void scenario_solveCaptchaTwiceCC() throws Exception {
        scenario_solveCaptchaTwice(true, true);
    }

    @Test
    public void scenario_solveCaptchaTwiceWC() throws Exception {
        scenario_solveCaptchaTwice(false, true);
    }

    @Test
    public void scenario_solveCaptchaTwiceCW() throws Exception {
        scenario_solveCaptchaTwice(true, false);
    }

    @Test
    public void scenario_solveCaptchaTwiceWW() throws Exception {
        scenario_solveCaptchaTwice(false, false);
    }

    @Test
    public void getCaptchaImageParameters_getMultipleTimes() throws Exception {
        KeyPair keyPair = sut.createClient();
        CaptchaRequest captchaRequest = sut.createCaptcha(keyPair.publicKey).getOrNull();
        CaptchaImageParameters imageParameters = Eithers.getOrThrow(sut.getCaptchaImageParameters(keyPair.publicKey,
                captchaRequest.token));

        boolean allAreEqual = IntStream.range(0, 1000).boxed()
                .map(i -> sut.getCaptchaImageParameters(keyPair.publicKey, captchaRequest.token))
                .map(Either::getOrNull)
                .allMatch(imageParameters::equals);

        Assert.assertTrue(allAreEqual);
        Assert.assertEquals(captchaRequest.answer, imageParameters.text);
    }
}