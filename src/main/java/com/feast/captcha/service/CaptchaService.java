package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.CaptchaImageParameters;
import com.feast.captcha.service.datatypes.CaptchaRequest;
import com.feast.captcha.service.datatypes.KeyPair;
import com.feast.captcha.service.datatypes.Token;
import io.atlassian.fugue.Either;

import java.util.UUID;

public interface CaptchaService {
    KeyPair createClient();
    Either<Exception, CaptchaRequest> createCaptcha(UUID publicKey);
    Either<Exception, CaptchaImageParameters> getCaptchaImageParameters(UUID publicKey, Token captchaToken);
    Either<Exception, Token> getSolutionToken(UUID publicKey, Token captchaToken, String answer);
    Either<Exception, Boolean> isAnswered(UUID secretKey, Token solutionToken);
}
