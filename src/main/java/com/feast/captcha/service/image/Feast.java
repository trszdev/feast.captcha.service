package com.feast.captcha.service.image;

import com.feast.captcha.service.Utils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.util.Random;

public class Feast {

    // https://stackoverflow.com/a/5055736
    public static ImageFilter getBulgeFilter(int randomIv, double power, double scale) {
        return image -> {
            Random random = new Random(randomIv);
            BufferedImage img = Utils.getDimensionsCopy(image);
            double cx = random.nextDouble();
            double cy = random.nextDouble();
            Utils.forEachPixel(image, (x, y) -> {
                double nx = (double) x / img.getWidth();
                double ny = (double) y / img.getHeight();
                double r = Math.sqrt((cx - nx)*(cx - nx) + (cy - ny)*(cy - ny));
                double a = Math.atan2(ny - cy, nx - cx);
                double rn = scale * Math.pow(r, power);
                double nx2 = rn * Math.cos(a) + cx;
                double ny2 = rn * Math.sin(a) + cy;
                int x2 = (int)(nx2 * img.getWidth());
                int y2 = (int)(ny2 * img.getHeight());
                if (x2 < img.getWidth() && y2 < img.getHeight() && x2 >=0 && y2 >= 0) {
                    img.setRGB(x, y, image.getRGB(x2, y2));
                }
            });
            return img;
        };
    }

    // https://stackoverflow.com/a/5057109
    public static ImageFilter getImplosionFilter(int randomIv, double scale) {
        return image -> {
            BufferedImage img = Utils.getDimensionsCopy(image);
            Random random = new Random(randomIv);
            double cx = random.nextDouble();
            double cy = random.nextDouble();
            Utils.forEachPixel(image, (x, y) -> {
                double nx = (double) x / img.getWidth();
                double ny = (double) y / img.getHeight();
                double dx = cx - nx;
                double dy = cy - ny;
                double len = Math.sqrt(dx*dx + dy*dy);
                double mx = scale * Math.log10(len) * (nx / len) + nx;
                double my = scale * Math.log10(len) * (ny / len) + ny;
                int x2 = (int)(mx * img.getWidth());
                int y2 = (int)(my * img.getHeight());
                if (x2 < img.getWidth() && y2 < img.getHeight() && x2 >=0 && y2 >= 0) {
                    img.setRGB(x, y, image.getRGB(x2, y2));
                }
            });
            return img;
        };
    }

    public static ImageTextGenerator getImageTextGenerator(int randomIv, double constOffset, double randomOffset) {
        return (text, options) -> {
            Random random = new Random(randomIv);
            BufferedImage image = new BufferedImage(options.imageSize.width, options.imageSize.height,
                    BufferedImage.TYPE_BYTE_BINARY);
            Graphics2D graphics2D = image.createGraphics();
            char[] chars = text.toCharArray();
            graphics2D.setColor(Color.white);
            Font font = Utils.randomPick(random, options.fonts);
            graphics2D.setFont(font);
            double offset = 0;
            for (int i = 0; i < chars.length; i++) {

                Rectangle2D rectangle2D = font.getStringBounds(chars, i, i+1, graphics2D.getFontRenderContext());
                offset += rectangle2D.getWidth() * random.nextDouble() * randomOffset;
                graphics2D.translate(offset, rectangle2D.getHeight() * (.65 + random.nextDouble() * .01));
                graphics2D.rotate(Utils.getRandomRadians(random, 10));
                offset += rectangle2D.getWidth() * constOffset;
                //graphics2D.setFont(font);
                graphics2D.drawChars(chars, i, 1, 0, 0);
                graphics2D.setTransform(new AffineTransform());
            }
            return image;
        };
    }

    // http://grepcode.com/file/repo1.maven.org/maven2/com.jhlabs/filters/2.0.235/com/jhlabs/image/InvertFilter.java
    public final static ImageFilter invertFilter = image -> {
        BufferedImage result = Utils.getDimensionsCopy(image);
        RGBImageFilter rgbImageFilter = new RGBImageFilter() {
            @Override
            public int filterRGB(int x, int y, int rgb) {
                int a = rgb & 0xff000000;
                return a | (~rgb & 0x00ffffff);
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), rgbImageFilter);
        Image inverted = Toolkit.getDefaultToolkit().createImage(ip);
        result.createGraphics().drawImage(inverted, 0, 0, null);
        return result;
    };

    public static ImageTextGenerator getCaptchaGenerator(int randomIv) {
        return (text, options) -> {
            Random random = Utils.getSecureRandom(randomIv);
            ImageTextGenerator imageTextGenerator = getImageTextGenerator(random.nextInt(), .65, .1);
            ImageFilter bulgeFilter = getBulgeFilter(random.nextInt(), 1.25, 1.3);
            BufferedImage image = imageTextGenerator.getTextImage(text, options);
            image = bulgeFilter.apply(image);
            image = invertFilter.apply(image);
            return image;
        };
    }
}
