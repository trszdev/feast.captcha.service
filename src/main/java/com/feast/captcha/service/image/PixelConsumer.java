package com.feast.captcha.service.image;

@FunctionalInterface
public interface PixelConsumer {
    void consume(int x, int y);
}
