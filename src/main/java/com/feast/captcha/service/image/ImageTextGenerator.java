package com.feast.captcha.service.image;

import com.feast.captcha.service.datatypes.ImageTextOptions;

import java.awt.image.BufferedImage;

@FunctionalInterface
public interface ImageTextGenerator {
    BufferedImage getTextImage(String text, ImageTextOptions options);
}
