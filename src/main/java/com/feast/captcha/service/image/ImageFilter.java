package com.feast.captcha.service.image;

import java.awt.image.BufferedImage;
import java.util.function.Function;

@FunctionalInterface
public interface ImageFilter extends Function<BufferedImage, BufferedImage> {

}
