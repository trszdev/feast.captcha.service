package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.CipherCaptchaServiceOptions;
import com.feast.captcha.service.datatypes.ImageSize;
import com.feast.captcha.service.datatypes.ImageTextOptions;
import com.google.common.collect.HashBiMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private final static String alphabet = "qwertyuiopasdfghjklzxcvbnm0123456789";

    @Bean("isProduction")
    boolean isProduction() {
        return System.getProperty("production") != null;
    }

    @Bean
    Function<Integer, String> tokenGenerator() {
        return Utils.getTokenGenerator(alphabet);
    }

    @Bean
    ImageTextOptions imageTextOptions() {
        return new ImageTextOptions(Utils.getFonts(80f, alphabet), new ImageSize(600, 110));
    }

    @Bean
    Supplier<Long> currentSeconds() {
        return () -> TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }

    @Bean
    CipherCaptchaServiceOptions captchaServiceOptions() {
        String ttl = System.getProperty("ttl");
        int ttlSeconds = ttl == null ? 600 : Integer.parseInt(ttl);
        if (ttlSeconds <= 0)
            ttlSeconds = 600;
        return new CipherCaptchaServiceOptions(ttlSeconds, 10, Utils.generateUuid(), HashBiMap.create());
    }

    @Override
    protected final SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
