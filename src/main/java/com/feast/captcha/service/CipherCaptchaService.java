package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.*;
import com.google.common.collect.BiMap;
import com.google.common.primitives.Bytes;
import io.atlassian.fugue.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.security.KeyException;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

@Service
public class CipherCaptchaService implements CaptchaService {
    private final int ttlSeconds;
    private final int captchaLength;
    private final UUID appKey;

    private final BiMap<UUID, UUID> keys;

    private final Function<Integer, String> answerGenerator;
    private final Supplier<Long> currentSeconds;
    private final CipherSuite cipherSuite;

    @Autowired
    public CipherCaptchaService(Function<Integer, String> answerGenerator, Supplier<Long> currentSeconds,
                                CipherSuite cipherSuite, CipherCaptchaServiceOptions options) {
        this.ttlSeconds = options.ttlSeconds;
        this.captchaLength = options.captchaLength;
        this.appKey = options.appKey;
        this.keys = options.keys;
        this.answerGenerator = answerGenerator;
        this.currentSeconds = currentSeconds;
        this.cipherSuite = cipherSuite;
    }

    @Override
    public KeyPair createClient() {
        UUID publicKey = Utils.generateUuid();
        UUID privateKey = Utils.generateUuid();
        keys.put(publicKey, privateKey);
        return new KeyPair(publicKey, privateKey);
    }

    @Override
    public Either<Exception, CaptchaRequest> createCaptcha(UUID publicKey) {
        UUID secretKey = keys.getOrDefault(publicKey, null);
        if (secretKey == null)
            return Either.left(new KeyException("No such public key"));
        String captchaAnswer = answerGenerator.apply(captchaLength);
        byte[] plain = Bytes.concat(
                Utils.randomBytes(4),
                Utils.pack(currentSeconds.get() + ttlSeconds),
                Utils.pack(captchaAnswer)
        );
        return cipherSuite.encryptWithTtl(plain, secretKey, ttlSeconds)
                .flatMap(semiCipher -> cipherSuite.encrypt(semiCipher, appKey))
                .map(Token::new)
                .map(token -> new CaptchaRequest(captchaAnswer, token));
    }

    @Override
    public Either<Exception, CaptchaImageParameters> getCaptchaImageParameters(UUID publicKey, Token captchaToken) {
        UUID secretKey = keys.getOrDefault(publicKey, null);
        if (secretKey == null)
            return Either.left(new KeyException("No such public key"));
        return cipherSuite.decrypt(captchaToken.bytes, appKey)
                .flatMap(semiPlain -> cipherSuite.decryptWithTtl(semiPlain, secretKey))
                .map(plain -> {
                    ByteBuffer byteBuffer = ByteBuffer.wrap(plain);
                    int randomIv = byteBuffer.getInt();
                    byteBuffer.getLong();
                    String captchaAnswer = Utils.unpack(Utils.getRemaining(byteBuffer));
                    return new CaptchaImageParameters(captchaAnswer, randomIv);
                });
    }

    @Override
    public Either<Exception, Token> getSolutionToken(UUID publicKey, Token captchaToken, String answer) {
        UUID secretKey = keys.getOrDefault(publicKey, null);
        if (secretKey == null)
            return Either.left(new KeyException("No such public key"));

        return cipherSuite.decrypt(captchaToken.bytes, appKey)
                .flatMap(semiPlain -> cipherSuite.decryptOnce(semiPlain, secretKey))
                .flatMap(plain -> {
                    ByteBuffer byteBuffer = ByteBuffer.wrap(plain);
                    byteBuffer.getInt();
                    int ttl = (int)(byteBuffer.getLong() - currentSeconds.get());
                    String captchaAnswer = Utils.unpack(Utils.getRemaining(byteBuffer));
                    byte[] newPlain = Bytes.concat(
                            Utils.pack(captchaAnswer.equals(answer)),
                            Utils.randomBytes(8)
                    );
                    return cipherSuite.encryptWithTtl(newPlain, publicKey, ttl)
                            .flatMap(semiCipher -> cipherSuite.encrypt(semiCipher, appKey))
                            .map(Token::new);
                });
    }

    @Override
    public Either<Exception, Boolean> isAnswered(UUID secretKey, Token solutionToken) {
        UUID publicKey = keys.inverse().getOrDefault(secretKey, null);
        if (secretKey == null)
            return Either.left(new KeyException("No such secret key"));
        return cipherSuite.decrypt(solutionToken.bytes, appKey)
                .flatMap(semiPlain -> cipherSuite.decryptOnce(semiPlain, publicKey))
                .flatMap(plain -> Either.right(plain[0] == 1));
    }
}
