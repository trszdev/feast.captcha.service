package com.feast.captcha.service;

import com.fasterxml.uuid.Generators;
import com.feast.captcha.service.image.PixelConsumer;
import com.google.common.collect.ImmutableList;
import com.google.common.io.BaseEncoding;
import com.google.common.primitives.Bytes;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;
import java.util.zip.CRC32;

public class Utils {

    public static String imageToBase64(BufferedImage bufferedImage) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        baos.flush();
        return BaseEncoding.base64().encode(baos.toByteArray());
    }

    public static BufferedImage getDeepCopy(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        WritableRaster raster = image.copyData(null);
        return new BufferedImage(cm, raster, cm.isAlphaPremultiplied(), null);
    }

    public static BufferedImage getDimensionsCopy(BufferedImage image) {
        return new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
    }

    public static double getRandomRadians(Random random, double maxDeviationDegree) {
        return Math.toRadians(maxDeviationDegree) * random.nextDouble() * (random.nextBoolean() ? -1 : 1);
    }

    public static <T> T randomPick(Random random, List<T> list) {
        return list.get(random.nextInt(list.size()));
    }

    public static ImmutableList<Font> getFonts(float fontSize, String alphabet) {
        Font[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        return ImmutableList.copyOf(Arrays.stream(fonts)
                .filter(font -> alphabet.chars().allMatch(font::canDisplay))
                .map(font -> font.deriveFont(Font.BOLD, fontSize))
                .iterator());
    }

    public static void forEachPixel(BufferedImage image, PixelConsumer pixelConsumer) {
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x  = 0; x < image.getWidth(); x++) {
                pixelConsumer.consume(x, y);
            }
        }
    }

    public static void checkNull(Object object, String objectName) {
        if (object == null)
            throw new NullPointerException(objectName + " is null");
    }

    public static SecureRandom getSecureRandom(int seed) {
        return new SecureRandom(ByteBuffer.allocate(4).putInt(seed).array());
    }

    public static Function<Integer, String> getTokenGenerator(String alphabet) {
        return length -> {
            Random random = new SecureRandom();
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
                chars[i] = alphabet.charAt(random.nextInt(alphabet.length()));
            return new String(chars);
        };
    }

    public static UUID generateUuid() {
        return Generators.randomBasedGenerator().generate();
    }

    public static byte[] pack(UUID uuid) {
        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.putLong(8, uuid.getLeastSignificantBits());
        buffer.putLong(0, uuid.getMostSignificantBits());
        return buffer.array();
    }

    public static byte[] pack(int value) {
        return ByteBuffer.allocate(4).putInt(value).array();
    }

    public static byte[] pack(long value) {
        return ByteBuffer.allocate(8).putLong(value).array();
    }

    public static byte[] randomBytes(int amount) {
        byte[] result = new byte[amount];
        new SecureRandom().nextBytes(result);
        return result;
    }

    public static byte[] pack(boolean value) {
        return new byte[] { (byte)(value ? 1 : 0) };
    }

    public static int randint() {
        return new SecureRandom().nextInt();
    }

    public static byte[] aesGcm(byte[] text, byte[] iv, byte[] key, boolean shouldEncrypt) {
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, iv);
            cipher.init(shouldEncrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE,
                    new SecretKeySpec(key, "AES"), gcmParameterSpec);
            return cipher.doFinal(text);
        }
        catch(Exception e) {
            return null;
        }
    }

    public static byte[] aesGcmEncrypt(byte[] plain, byte[] key) {
        byte[] iv = Utils.randomBytes(16);
        byte[] cipher = aesGcm(plain, iv, key, true);
        if (cipher == null)
            return null;
        return Bytes.concat(iv, cipher);
    }

    public static byte[] aesGcmDecrypt(byte[] cipher, byte[] key) {
        if (cipher.length <= 16)
            return null;
        byte[] iv = Arrays.copyOfRange(cipher, 0, 16);
        cipher = Arrays.copyOfRange(cipher, 16, cipher.length);
        return aesGcm(cipher, iv, key, false);
    }

    public static int crc32(byte[] buf) {
        CRC32 crc32 = new CRC32();
        crc32.update(buf);
        return (int) crc32.getValue();
    }

    public static byte[] pack(String text) {
        try {
            return text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getRemaining(ByteBuffer byteBuffer) {
        byte[] result = new byte[byteBuffer.remaining()];
        byteBuffer.get(result);
        return result;
    }

    public static String unpack(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
