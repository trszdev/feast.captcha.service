package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.base.Objects;

import java.util.UUID;

public class KeyPair {
    public final UUID publicKey;
    public final UUID secretKey;

    public KeyPair(UUID publicKey, UUID secretKey) {
        Utils.checkNull(publicKey, "publicKey");
        Utils.checkNull(secretKey, "secretKey");
        this.publicKey = publicKey;
        this.secretKey = secretKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyPair keyPair = (KeyPair) o;
        return Objects.equal(publicKey, keyPair.publicKey) &&
                Objects.equal(secretKey, keyPair.secretKey);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(publicKey, secretKey);
    }
}
