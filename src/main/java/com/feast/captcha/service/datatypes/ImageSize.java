package com.feast.captcha.service.datatypes;

import com.google.common.base.Objects;

public class ImageSize {
    public final int width;
    public final int height;

    public ImageSize(int width, int height) {
        if (width <= 0)
            throw new IllegalArgumentException("Width must be positive");
        if (height <= 0)
            throw new IllegalArgumentException("Height must be positive");
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageSize imageSize = (ImageSize) o;
        return width == imageSize.width &&
                height == imageSize.height;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(width, height);
    }
}
