package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import java.awt.*;

public class ImageTextOptions {
    public final ImmutableList<Font> fonts;
    public final ImageSize imageSize;

    public ImageTextOptions(ImmutableList<Font> fonts, ImageSize imageSize) {
        Utils.checkNull(fonts, "fonts");
        Utils.checkNull(imageSize, "imageSize");
        if (fonts.size() == 0)
            throw new IllegalArgumentException("fonts must contain at least 1 item");

        this.fonts = fonts;
        this.imageSize = imageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageTextOptions that = (ImageTextOptions) o;
        return Objects.equal(fonts, that.fonts) &&
                Objects.equal(imageSize, that.imageSize);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(fonts, imageSize);
    }
}
