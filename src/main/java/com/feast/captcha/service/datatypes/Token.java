package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.base.Objects;
import com.google.errorprone.annotations.Immutable;

@Immutable
public class Token {
    public final byte[] bytes;

    public Token(byte[] bytes) {
        Utils.checkNull(bytes, "bytes");
        this.bytes = bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return Objects.equal(bytes, token.bytes);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(bytes);
    }
}
