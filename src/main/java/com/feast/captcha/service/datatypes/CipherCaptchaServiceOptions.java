package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.collect.BiMap;

import java.util.UUID;

public class CipherCaptchaServiceOptions {
    public final int ttlSeconds;
    public final int captchaLength;
    public final UUID appKey;
    public final BiMap<UUID, UUID> keys;

    public CipherCaptchaServiceOptions(int ttlSeconds, int captchaLength, UUID appKey, BiMap<UUID, UUID> keys) {
        Utils.checkNull(appKey, "appKey");
        Utils.checkNull(keys, "keys");
        this.ttlSeconds = ttlSeconds;
        this.captchaLength = captchaLength;
        this.appKey = appKey;
        this.keys = keys;
    }
}
