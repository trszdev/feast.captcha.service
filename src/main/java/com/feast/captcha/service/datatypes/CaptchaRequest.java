package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.base.Objects;

public class CaptchaRequest {
    public final String answer;
    public final Token token;

    public CaptchaRequest(String answer, Token token) {
        Utils.checkNull(answer, "answer");
        Utils.checkNull(token, "token");
        this.answer = answer;
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaptchaRequest that = (CaptchaRequest) o;
        return Objects.equal(answer, that.answer) &&
                Objects.equal(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(answer, token);
    }
}
