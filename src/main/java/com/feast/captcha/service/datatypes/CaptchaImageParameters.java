package com.feast.captcha.service.datatypes;

import com.feast.captcha.service.Utils;
import com.google.common.base.Objects;

public class CaptchaImageParameters {
    public final String text;
    public final int randomIv;

    public CaptchaImageParameters(String text, int randomIv) {
        Utils.checkNull(text, "text");
        this.text = text;
        this.randomIv = randomIv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaptchaImageParameters that = (CaptchaImageParameters) o;
        return randomIv == that.randomIv &&
                Objects.equal(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(text, randomIv);
    }
}
