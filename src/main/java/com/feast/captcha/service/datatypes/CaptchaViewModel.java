package com.feast.captcha.service.datatypes;

import java.util.Random;

public class CaptchaViewModel {
    private int imageWidth;
    private int imageHeight;
    private float fontSize;
    private int randomIv;
    private String text;

    public void setFontSize(float fontSize) {
        this.fontSize = fontSize;
    }

    public float getFontSize() {
        return fontSize;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public void setRandomIv(int randomIv) {
        this.randomIv = randomIv;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public int getRandomIv() {
        return randomIv;
    }

    public String getText() {
        return text;
    }

    public CaptchaViewModel() {
        fontSize = 80f;
        randomIv = new Random().nextInt();
        imageHeight = 110;
        imageWidth = 600;
        text = "qwerty123456789";
    }
}
