package com.feast.captcha.service;

import io.atlassian.fugue.Either;

import java.util.UUID;

public interface CipherSuite {
    Either<Exception, byte[]> encrypt(byte[] plainText, UUID key);
    Either<Exception, byte[]> encryptWithTtl(byte[] plainText, UUID key, int ttlSeconds);
    Either<Exception, byte[]> decryptOnce(byte[] cipherText, UUID key);
    Either<Exception, byte[]> decrypt(byte[] cipherText, UUID key);
    Either<Exception, byte[]> decryptWithTtl(byte[] cipherText, UUID key);
}
