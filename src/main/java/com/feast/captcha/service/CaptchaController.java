package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.ImageTextOptions;
import com.feast.captcha.service.datatypes.KeyPair;
import com.feast.captcha.service.datatypes.Token;
import com.feast.captcha.service.image.Feast;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.BaseEncoding;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Eithers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.management.openmbean.KeyAlreadyExistsException;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.security.KeyException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

@RestController
public class CaptchaController {

    @Autowired
    @Qualifier("isProduction")
    private boolean isProduction;

    @Autowired
    private ImageTextOptions options;

    @Autowired
    private CaptchaService captchaService;

    private <X> Either<Exception, X> either(Supplier<X> supplier, String message) {
        try {
            return Either.right(supplier.get());
        } catch (Exception e) {
            return Either.left(new Exception(message, e));
        }
    }

    private Either<Exception, UUID> uuidFromString(String value) {
        return either(() -> UUID.fromString(value), "Unable to parse UUID");
    }

    private Either<Exception, Token> tokenFromBase64(String base64) {
        return either(() -> new Token(BaseEncoding.base64Url().decode(base64)),
                "Bad base64url sequence");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map handleException(Exception exception) {
        String message = exception.getMessage();
        return ImmutableMap.of("error", message == null ? "No message provided" : message);
    }

    @ExceptionHandler(KeyException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Map handleKeyException(KeyException exception) {
        return ImmutableMap.of("error", exception.getMessage());
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/client/register",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map postClientRegistration() {
        KeyPair keyPair = captchaService.createClient();
        return ImmutableMap.of(
                "secret", keyPair.secretKey,
                "public", keyPair.publicKey
        );
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/captcha/new",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map getNewCaptcha(@RequestParam("public") String publicKey) throws Exception {
        return Eithers.getOrThrow(uuidFromString(publicKey)
                .flatMap(captchaService::createCaptcha)
                .map(cr -> new HashMap() {{
                    put("request", BaseEncoding.base64Url().encode(cr.token.bytes));
                    if (!isProduction)
                        put("answer", cr.answer);
                }})
        );
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/captcha/image",
            produces = MediaType.IMAGE_PNG_VALUE)
    public void getCaptchaImage(@RequestParam("public") String publicKey,
                                @RequestParam("request") String requestId,
                                HttpServletResponse response) throws Exception {
        BufferedImage bufferedImage = Eithers.getOrThrow(tokenFromBase64(requestId)
                .flatMap(token -> uuidFromString(publicKey)
                    .flatMap(publicUuid -> captchaService.getCaptchaImageParameters(publicUuid, token)))
                .map(cip -> Feast.getCaptchaGenerator(cip.randomIv).getTextImage(cip.text, options)));
        response.setStatus(HttpServletResponse.SC_OK);
        ImageIO.write(bufferedImage, "png", response.getOutputStream());
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/captcha/solve",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map postCaptchaSolution(@RequestParam("public") String publicKey,
                                   @RequestParam("request") String requestId,
                                   @RequestParam("answer") String captchaAnswer) throws Exception {
        return Eithers.getOrThrow(uuidFromString(publicKey)
                .flatMap(publicUuid -> tokenFromBase64(requestId)
                    .flatMap(token -> captchaService.getSolutionToken(publicUuid, token, captchaAnswer)))
                .map(token -> ImmutableMap.of("response", BaseEncoding.base64Url().encode(token.bytes))));
    }


    @RequestMapping(
            method = RequestMethod.GET,
            value = "/captcha/verify",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map getCaptchaVerification(@RequestParam("secret") String secretKey,
                                      @RequestParam("response") String responseId) throws Exception {
        Either<Exception, Boolean> answer = uuidFromString(secretKey)
                .flatMap(secretUuid -> tokenFromBase64(responseId)
                        .flatMap(token -> captchaService.isAnswered(secretUuid, token)));
        if (answer.isLeft() && answer.left().get() instanceof KeyAlreadyExistsException)
            throw answer.left().get();
        // immutable map doesn't support null type
        return new HashMap() {{
           put("success", answer.getOrElse(false));
           put("errorCode", answer.isLeft() ? answer.left().get().getMessage() : null);
        }};
    }
}
