package com.feast.captcha.service;

import com.feast.captcha.service.datatypes.CaptchaViewModel;
import com.feast.captcha.service.image.Feast;
import com.feast.captcha.service.datatypes.ImageSize;
import com.feast.captcha.service.datatypes.ImageTextOptions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@Controller
public class IndexController {
    @RequestMapping(value = "/",  method = RequestMethod.GET)
    public ModelAndView index(@ModelAttribute CaptchaViewModel viewModel) throws IOException {
        ImageTextOptions options = new ImageTextOptions(Utils.getFonts(viewModel.getFontSize(), viewModel.getText()),
                new ImageSize(viewModel.getImageWidth(), viewModel.getImageHeight()));
        BufferedImage image = Feast.getCaptchaGenerator(viewModel.getRandomIv()).getTextImage(viewModel.getText(), options);
        ModelAndView modelAndView = new ModelAndView("index", "model", viewModel);
        modelAndView.addObject("imageBase64", Utils.imageToBase64(image));
        modelAndView.addObject("randomIv2", new Random().nextInt());
        return modelAndView;
    }
}
