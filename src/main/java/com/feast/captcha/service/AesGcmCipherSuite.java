package com.feast.captcha.service;

import com.google.common.primitives.Bytes;
import io.atlassian.fugue.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.openmbean.KeyAlreadyExistsException;
import javax.naming.TimeLimitExceededException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

@Service
public class AesGcmCipherSuite implements CipherSuite {
    private final Supplier<Long> currentSeconds;
    private final Map<Integer, Long> usedTokens;


    public AesGcmCipherSuite(Supplier<Long> currentSeconds, Map<Integer, Long> usedTokens) {
        this.currentSeconds = currentSeconds;
        this.usedTokens = usedTokens;
    }

    @Autowired
    public AesGcmCipherSuite(Supplier<Long> currentSeconds) {
        this(currentSeconds, new HashMap<>());
    }

    @Override
    public Either<Exception, byte[]> encrypt(byte[] plainText, UUID key) {
        byte[] cipherText = Utils.aesGcmEncrypt(plainText, Utils.pack(key));
        return Either.right(cipherText);
    }

    @Override
    public Either<Exception, byte[]> encryptWithTtl(byte[] plainText, UUID key, int ttlSeconds) {
        byte[] plain = Bytes.concat(
                Utils.pack(currentSeconds.get() + ttlSeconds),
                plainText
        );
        return encrypt(plain, key);
    }

    @Override
    public Either<Exception, byte[]> decryptOnce(byte[] cipherText, UUID key) {
        return decryptWithTtlAll(cipherText, key)
                .map(plain -> {
                    ByteBuffer byteBuffer = ByteBuffer.wrap(plain);
                    long timeStamp = byteBuffer.getLong();
                    usedTokens.put(Utils.crc32(cipherText), timeStamp);
                    return Utils.getRemaining(byteBuffer);
                });
    }

    private Either<Exception, byte[]> decryptWithTtlAll(byte[] cipherText, UUID key) {
        return decrypt(cipherText, key).flatMap(plain -> {
            if (usedTokens.containsKey(Utils.crc32(cipherText)))
                return Either.left(new KeyAlreadyExistsException("Token was already used"));
            ByteBuffer byteBuffer = ByteBuffer.wrap(plain);
            long timestamp = byteBuffer.getLong();
            if (currentSeconds.get() >= timestamp)
                return Either.left(new TimeLimitExceededException("Token ttl exceeded"));
            return Either.right(plain);
        });
    }

    public Either<Exception, byte[]> decryptWithTtl(byte[] cipherText, UUID key) {
        return decryptWithTtlAll(cipherText, key)
                .map(plain -> Arrays.copyOfRange(plain, 8, plain.length));
    }

    @Override
    public Either<Exception, byte[]> decrypt(byte[] cipherText, UUID key) {
        byte[] plain = Utils.aesGcmDecrypt(cipherText, Utils.pack(key));
        if (plain == null)
            return Either.left(new InvalidKeyException("Corrupted token"));
        return Either.right(plain);
    }
}
